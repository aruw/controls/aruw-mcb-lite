/*
 * Copyright (c) 2023-2024 Advanced Robotics at the University of Washington <robomstr@uw.edu>
 *
 * This file is part of aruw-mcb-lite.
 *
 * aruw-mcb-lite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aruw-mcb-lite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aruw-mcb-lite.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "imu_handler.hpp"

#include "tap/communication/serial/dji_serial.hpp"
#include "tap/drivers.hpp"

#include "modm/architecture/interface/can_message.hpp"

namespace src::virtualMCB
{
IMUHandler::IMUHandler(tap::Drivers* drivers)
    : drivers(drivers),
      imu(&(drivers->mpu6500)),
      currentIMUMessage(),
      currentIMUData()
{
    currentIMUMessage.messageType = IMU_MESSAGE;
}

void IMUHandler::refresh()
{
    currentIMUData = {
        .pitch = imu->getPitch(),
        .roll = imu->getRoll(),
        .yaw = imu->getYaw(),
        .Gx = imu->getGx(),
        .Gy = imu->getGy(),
        .Gz = imu->getGz(),
        .Ax = imu->getAx(),
        .Ay = imu->getAy(),
        .Az = imu->getAz(),
        .imuState = imu->getImuState(),
        .temperature = imu->getTemp()};

    memcpy(currentIMUMessage.data, &currentIMUData, sizeof(IMUMessage));
    currentIMUMessage.setCRC16();
}

void IMUHandler::calibrateIMU() { imu->requestCalibration(); }

}  // namespace src::virtualMCB
