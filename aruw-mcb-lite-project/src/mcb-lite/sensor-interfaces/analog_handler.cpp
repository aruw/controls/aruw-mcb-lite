/*
 * Copyright (c) 2023-2024 Advanced Robotics at the University of Washington <robomstr@uw.edu>
 *
 * This file is part of aruw-mcb-lite.
 *
 * aruw-mcb-lite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aruw-mcb-lite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aruw-mcb-lite.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "analog_handler.hpp"

#include "tap/drivers.hpp"

namespace src::virtualMCB
{
AnalogHandler::AnalogHandler(tap::Drivers* drivers)
    : drivers(drivers),
      analogPinData(),
      analogPinMessage()
{
    analogPinMessage.messageType = ANALOG_PIN_READ_MESSAGE;
}

void AnalogHandler::refresh()
{
    analogPinData = {
        .SPinValue = drivers->analog.read(tap::gpio::Analog::Pin::S),
        .TPinValue = drivers->analog.read(tap::gpio::Analog::Pin::T),
        .UPinValue = drivers->analog.read(tap::gpio::Analog::Pin::U),
        .VPinValue = drivers->analog.read(tap::gpio::Analog::Pin::V),
        .OLEDPinValue = drivers->analog.read(tap::gpio::Analog::Pin::OledJoystick)};

    memcpy(analogPinMessage.data, &analogPinData, sizeof(analogPinData));
    analogPinMessage.setCRC16();
}

}  // namespace src::virtualMCB
