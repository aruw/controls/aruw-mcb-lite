/*
 * Copyright (c) 2023-2024 Advanced Robotics at the University of Washington <robomstr@uw.edu>
 *
 * This file is part of aruw-mcb-lite.
 *
 * aruw-mcb-lite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aruw-mcb-lite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aruw-mcb-lite.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DIGITAL_HANDLER_HPP_
#define DIGITAL_HANDLER_HPP_

#include "tap/communication/gpio/digital.hpp"
#include "tap/communication/serial/dji_serial.hpp"
#include "tap/drivers.hpp"

#include "mcb-lite/message_types.hpp"

namespace src::virtualMCB
{
/**
 * This class reports the current sensor reading in mA. To use it, pass in the digital that reads
 * the current data and then call getMessage() to get the current reading.
 */
class DigitalHandler
{
public:
    DigitalHandler(tap::Drivers* drivers);

    void refresh();

    tap::Drivers* drivers;
    DigitalInputPinMessage digitalPinData;
    tap::communication::serial::DJISerial::SerialMessage<sizeof(DigitalInputPinMessage)>
        digitalPinMessage;
};

}  // namespace src::virtualMCB

#endif
