/*
 * Copyright (c) 2023-2024 Advanced Robotics at the University of Washington <robomstr@uw.edu>
 *
 * This file is part of aruw-mcb-lite.
 *
 * aruw-mcb-lite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aruw-mcb-lite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aruw-mcb-lite.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IMU_HANDLER_HPP_
#define IMU_HANDLER_HPP_

#include "tap/communication/serial/dji_serial.hpp"
#include "tap/drivers.hpp"

#include "mcb-lite/message_types.hpp"

namespace src::virtualMCB
{
/**
 * This class provides current IMU readings in degrees and degrees per second.
 */
class IMUHandler
{
public:
    IMUHandler(tap::Drivers* drivers);

    void calibrateIMU();

    void refresh();

    tap::Drivers* drivers;

    tap::communication::sensors::imu::mpu6500::Mpu6500* imu;

    tap::communication::serial::DJISerial::SerialMessage<sizeof(IMUMessage)> currentIMUMessage;

    IMUMessage currentIMUData;
};

}  // namespace src::virtualMCB

#endif
