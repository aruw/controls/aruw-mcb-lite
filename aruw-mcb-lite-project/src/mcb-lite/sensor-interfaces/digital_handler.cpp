/*
 * Copyright (c) 2023-2024 Advanced Robotics at the University of Washington <robomstr@uw.edu>
 *
 * This file is part of aruw-mcb-lite.
 *
 * aruw-mcb-lite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aruw-mcb-lite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aruw-mcb-lite.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "digital_handler.hpp"

#include "tap/drivers.hpp"

namespace src::virtualMCB
{
DigitalHandler::DigitalHandler(tap::Drivers* drivers)
    : drivers(drivers),
      digitalPinData(),
      digitalPinMessage()
{
    digitalPinMessage.messageType = DIGITAL_PID_READ_MESSAGE;
}

void DigitalHandler::refresh()
{
    digitalPinData = {
        .BPinValue = drivers->digital.read(tap::gpio::Digital::InputPin::B),
        .CPinValue = drivers->digital.read(tap::gpio::Digital::InputPin::C),
        .DPinValue = drivers->digital.read(tap::gpio::Digital::InputPin::D),
        .ButtonPinValue = drivers->digital.read(tap::gpio::Digital::InputPin::Button)};
    memcpy(digitalPinMessage.data, &digitalPinData, sizeof(digitalPinData));
    digitalPinMessage.setCRC16();
}

}  // namespace src::virtualMCB
