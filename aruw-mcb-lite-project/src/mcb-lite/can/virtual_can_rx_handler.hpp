/*
 * Copyright (c) 2023-2024 Advanced Robotics at the University of Washington <robomstr@uw.edu>
 *
 * This file is part of aruw-mcb-lite.
 *
 * aruw-mcb-lite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aruw-mcb-lite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aruw-mcb-lite.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VIRTUAL_CAN_RX_HANDLER_HPP_
#define VIRTUAL_CAN_RX_HANDLER_HPP_

#include "tap/communication/can/can_rx_handler.hpp"
#include "tap/communication/serial/dji_serial.hpp"
#include "tap/drivers.hpp"
#include "tap/motor/dji_motor.hpp"

#include "mcb-lite/message_types.hpp"
#include "modm/architecture/interface/can_message.hpp"

namespace src::virtualMCB
{
/**
 * This class is used to handle the reception of CAN messages from the virtual MCB.
 * It packages incoming CAN messages to be sent to the virtual MCB.
 */
class VirtualCanRxHandler : public tap::can::CanRxHandler
{
public:
    VirtualCanRxHandler(tap::Drivers* drivers);

    void refresh();

    tap::Drivers* drivers;
    modm::can::Message can1Message;
    modm::can::Message can2Message;
    uint32_t can1GotRead = 0;
    uint32_t can2GotRead = 0;
    uint32_t polled = 0;

    uint8_t can1Bitmap;
    uint8_t can1Data[64];
    uint8_t can2Bitmap;
    uint8_t can2Data[64];

    tap::communication::serial::DJISerial::SerialMessage<sizeof(can1Data) + sizeof(can1Bitmap)>
        can1SerialMessage;
    tap::communication::serial::DJISerial::SerialMessage<sizeof(can2Data) + sizeof(can2Bitmap)>
        can2SerialMessage;
};

}  // namespace src::virtualMCB

#endif
