/*
 * Copyright (c) 2023-2024 Advanced Robotics at the University of Washington <robomstr@uw.edu>
 *
 * This file is part of aruw-mcb-lite.
 *
 * aruw-mcb-lite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aruw-mcb-lite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aruw-mcb-lite.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "virtual_can_rx_handler.hpp"

namespace src::virtualMCB
{
VirtualCanRxHandler::VirtualCanRxHandler(tap::Drivers* drivers)
    : CanRxHandler(drivers),
      drivers(drivers),
      can1Message(),
      can2Message()
{
    can1SerialMessage.messageType = CANBUS1_MESSAGE;
    can2SerialMessage.messageType = CANBUS2_MESSAGE;
}

void VirtualCanRxHandler::refresh()
{
    polled++;
    modm::can::Message rxMessage;
    if (drivers->can.getMessage(tap::can::CanBus::CAN_BUS1, &rxMessage))
    {
        can1GotRead++;
        memcpy(&can1Message, &rxMessage, sizeof(modm::can::Message));
        memcpy(
            &can1Data[(rxMessage.getIdentifier() - tap::motor::MotorId::MOTOR1) * 8],
            &rxMessage.data,
            sizeof(rxMessage.data));

        memcpy(&can1SerialMessage.data, &can1Data, sizeof(can1Data));

        can1Bitmap = can1Bitmap | (1 << (rxMessage.getIdentifier() - tap::motor::MotorId::MOTOR1));
        memcpy(&can1SerialMessage.data[sizeof(can1Data)], &can1Bitmap, sizeof(can1Bitmap));

        can1SerialMessage.setCRC16();
    }

    if (drivers->can.getMessage(tap::can::CanBus::CAN_BUS2, &rxMessage))
    {
        can2GotRead++;
        memcpy(&can2Message, &rxMessage, sizeof(modm::can::Message));
        memcpy(
            &can2Data[(rxMessage.getIdentifier() - tap::motor::MotorId::MOTOR1) * 8],
            &rxMessage.data,
            sizeof(rxMessage.data));

        memcpy(&can2SerialMessage.data, &can2Data, sizeof(can2Data));

        can2Bitmap = can2Bitmap | (1 << (rxMessage.getIdentifier() - tap::motor::MotorId::MOTOR1));
        memcpy(&can2SerialMessage.data[sizeof(can2Data)], &can2Bitmap, sizeof(can2Bitmap));

        can2SerialMessage.setCRC16();
    }
}

}  // namespace src::virtualMCB
