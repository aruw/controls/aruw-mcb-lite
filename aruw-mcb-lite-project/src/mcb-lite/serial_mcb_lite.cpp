/*
 * Copyright (c) 2023-2024 Advanced Robotics at the University of Washington <robomstr@uw.edu>
 *
 * This file is part of aruw-mcb-lite.
 *
 * aruw-mcb-lite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aruw-mcb-lite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aruw-mcb-lite.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "serial_mcb_lite.hpp"

#include "tap/communication/can/can.hpp"
#include "tap/communication/can/can_bus.hpp"
#include "tap/communication/serial/uart.hpp"
#include "tap/drivers.hpp"

using namespace tap::communication::serial;
using namespace tap::gpio;

namespace src::virtualMCB
{
SerialMCBLite::SerialMCBLite(tap::Drivers* drivers, tap::communication::serial::Uart::UartPort port)
    : DJISerial(drivers, port),
      port(port),
      imuHandler(drivers),
      canRxHandler(drivers),
      analogHandler(drivers),
      digitalHandler(drivers),
      ledStatus()
{
}

void SerialMCBLite::messageReceiveCallback(const ReceivedSerialMessage& completeMessage)
{
    receivedMessageCount++;
    switch (completeMessage.messageType)
    {
        case CALIBRATE_IMU_MESSAGE:
            receivedIMUMessageCount++;
            imuHandler.calibrateIMU();
            break;
        case CANBUS1_MESSAGE:
            receivedCanbus1Message++;
            sendMotorData(completeMessage);
            break;
        case CANBUS2_MESSAGE:
            receivedCanbus2Message++;
            sendMotorData(completeMessage);
            break;
        case DIGITAL_OUTPUT_MESSAGE:
            receivedDigitalOutputMessage++;
            processDigitalOutputMessage(completeMessage);
            break;
        case DIGITAL_PIN_MODE_MESSAGE:
            receivedDigitalPinModeMessage++;
            processDigitalPinModeMessage(completeMessage);
            break;
        case PWM_PIN_DUTY_MESSAGE:
            receivedPWMPinDutyMessage++;
            processPWMPinDutyMessage(completeMessage);
            break;
        case PWM_TIMER_FREQUENCY_MESSAGE:
            receivedPWMTimerFrequencyMessage++;
            processPWMTimerFrequencyMessage(completeMessage);
            break;
        case PWM_TIMER_STARTED_MESSAGE:
            receivedPWMTimerStartedMessage++;
            processPWMTimerStartedMessage(completeMessage);
            break;
        case LED_CONTROL_MESSAGE:
            receievedLEDMessage++;
            processLEDMessage(completeMessage);
            break;
        default:
            break;
    }
}

void SerialMCBLite::sendMotorData(const ReceivedSerialMessage& completeMessage)
{
    memcpy(&message, completeMessage.data, sizeof(modm::can::Message));
    if (completeMessage.messageType == CANBUS1_MESSAGE)
    {
        drivers->can.sendMessage(tap::can::CanBus::CAN_BUS1, message);
    }
    else
    {
        drivers->can.sendMessage(tap::can::CanBus::CAN_BUS2, message);
    }
}

void SerialMCBLite::processDigitalOutputMessage(const ReceivedSerialMessage& completeMessage)
{
    memcpy(&digitalOutputMessage, completeMessage.data, sizeof(DigitalOutputPinMessage));
    drivers->digital.set(Digital::OutputPin::E, digitalOutputMessage.EPinValue);
    drivers->digital.set(Digital::OutputPin::F, digitalOutputMessage.FPinValue);
    drivers->digital.set(Digital::OutputPin::G, digitalOutputMessage.GPinValue);
    drivers->digital.set(Digital::OutputPin::H, digitalOutputMessage.HPinValue);
    drivers->digital.set(Digital::OutputPin::Laser, digitalOutputMessage.LaserPinValue);
}

void SerialMCBLite::processDigitalPinModeMessage(const ReceivedSerialMessage& completeMessage)
{
    memcpy(&digitalPinModeMessage, completeMessage.data, sizeof(DigitalPinModeMessage));
    drivers->digital.configureInputPullMode(
        Digital::InputPin::B,
        static_cast<Digital::InputPullMode>(digitalPinModeMessage.BPinMode));
    drivers->digital.configureInputPullMode(
        Digital::InputPin::C,
        static_cast<Digital::InputPullMode>(digitalPinModeMessage.CPinMode));
    drivers->digital.configureInputPullMode(
        Digital::InputPin::D,
        static_cast<Digital::InputPullMode>(digitalPinModeMessage.DPinMode));
    drivers->digital.configureInputPullMode(
        Digital::InputPin::Button,
        static_cast<Digital::InputPullMode>(digitalPinModeMessage.ButtonPinMode));
}

void SerialMCBLite::processPWMPinDutyMessage(const ReceivedSerialMessage& completeMessage)
{
    memcpy(&pwmPinDutyMessage, completeMessage.data, sizeof(PWMPinDutyMessage));
    drivers->pwm.write(pwmPinDutyMessage.WPinDuty, Pwm::Pin::W);
    drivers->pwm.write(pwmPinDutyMessage.XPinDuty, Pwm::Pin::X);
    drivers->pwm.write(pwmPinDutyMessage.YPinDuty, Pwm::Pin::Y);
    drivers->pwm.write(pwmPinDutyMessage.ZPinDuty, Pwm::Pin::Z);
    drivers->pwm.write(pwmPinDutyMessage.BuzzerPinDuty, Pwm::Pin::Buzzer);
    drivers->pwm.write(pwmPinDutyMessage.IMUHeaterPinDuty, Pwm::Pin::ImuHeater);
}

void SerialMCBLite::processPWMTimerFrequencyMessage(const ReceivedSerialMessage& completeMessage)
{
    memcpy(&pwmTimerFrequencyMessage, completeMessage.data, sizeof(PWNTimerFrequencyMessage));
    if (pwmTimerFrequencyMessage.timer8Frequency != 0)
    {
        drivers->pwm.setTimerFrequency(
            Pwm::Timer::TIMER8,
            pwmTimerFrequencyMessage.timer8Frequency);
    }

    if (pwmTimerFrequencyMessage.timer12Frequency != 0)
    {
        drivers->pwm.setTimerFrequency(
            Pwm::Timer::TIMER12,
            pwmTimerFrequencyMessage.timer12Frequency);
    }

    if (pwmTimerFrequencyMessage.timer3Frequency != 0)
    {
        drivers->pwm.setTimerFrequency(
            Pwm::Timer::TIMER3,
            pwmTimerFrequencyMessage.timer3Frequency);
    }
}

void SerialMCBLite::processPWMTimerStartedMessage(const ReceivedSerialMessage& completeMessage)
{
    memcpy(&pwmTimerStartedMessage, completeMessage.data, sizeof(PWMTimerStartedMessage));
    if (pwmTimerStartedMessage.timer8Started)
    {
        drivers->pwm.start(Pwm::Timer::TIMER8);
    }
    else
    {
        drivers->pwm.pause(Pwm::Timer::TIMER8);
    }
    if (pwmTimerStartedMessage.timer12Started)
    {
        drivers->pwm.start(Pwm::Timer::TIMER12);
    }
    else
    {
        drivers->pwm.pause(Pwm::Timer::TIMER12);
    }
    if (pwmTimerStartedMessage.timer3Started)
    {
        drivers->pwm.start(Pwm::Timer::TIMER3);
    }
    else
    {
        drivers->pwm.pause(Pwm::Timer::TIMER3);
    }
}

void SerialMCBLite::processLEDMessage(const ReceivedSerialMessage& completeMessage)
{
    memcpy(&ledStatus, completeMessage.data, sizeof(LEDControlMessage));
    for (uint8_t i = 0; i < sizeof(LEDControlMessage); i++)
    {
        drivers->leds.set(static_cast<Leds::LedPin>(i), completeMessage.data[i]);
    }
}

void SerialMCBLite::initialize()
{
    switch (this->port)
    {
        case Uart::UartPort::Uart1:
            drivers->uart.init<Uart::UartPort::Uart1, UART_BAUDRATE>();
            break;
        case Uart::UartPort::Uart2:
            drivers->uart.init<Uart::UartPort::Uart2, UART_BAUDRATE>();
            break;
        case Uart::UartPort::Uart3:
            drivers->uart.init<Uart::UartPort::Uart3, UART_BAUDRATE>();
            break;
        case Uart::UartPort::Uart6:
            drivers->uart.init<Uart::UartPort::Uart6, UART_BAUDRATE>();
            break;
        case Uart::UartPort::Uart7:
            drivers->uart.init<Uart::UartPort::Uart7, UART_BAUDRATE>();
            break;
        case Uart::UartPort::Uart8:
            drivers->uart.init<Uart::UartPort::Uart8, UART_BAUDRATE>();
            break;
        default:
            break;
    }
}

void SerialMCBLite::sendData()
{
    sendingAMessage++;
    if (drivers->uart.isWriteFinished(port))
    {
        sentAMessage++;
        drivers->uart.write(
            port,
            reinterpret_cast<uint8_t*>(&imuHandler.currentIMUMessage),
            sizeof(imuHandler.currentIMUMessage));  // 53

        drivers->uart.write(
            port,
            reinterpret_cast<uint8_t*>(&canRxHandler.can1SerialMessage),
            sizeof(canRxHandler.can1SerialMessage));  // 74

        // drivers->uart.write(
        //     port,
        //     reinterpret_cast<uint8_t*>(&canRxHandler.can2SerialMessage),
        //     sizeof(canRxHandler.can2SerialMessage));  // 74

        drivers->uart.write(
            port,
            reinterpret_cast<uint8_t*>(&analogHandler.analogPinMessage),
            sizeof(analogHandler.analogPinMessage));  // 19

        // drivers->uart.write(
        //     port,
        //     reinterpret_cast<uint8_t*>(&digitalHandler.digitalPinMessage),
        //     sizeof(digitalHandler.digitalPinMessage));  // 13
    }
}

void SerialMCBLite::updateIO()
{
    if (updateIObool)
    {
        imuHandler.refresh();
        canRxHandler.refresh();
        analogHandler.refresh();
        digitalHandler.refresh();
    }
}

}  // namespace src::virtualMCB
