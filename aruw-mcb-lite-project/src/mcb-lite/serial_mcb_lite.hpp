/*
 * Copyright (c) 2023-2024 Advanced Robotics at the University of Washington <robomstr@uw.edu>
 *
 * This file is part of aruw-mcb-lite.
 *
 * aruw-mcb-lite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aruw-mcb-lite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aruw-mcb-lite.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SERIAL_MCB_LITE_HPP_
#define SERIAL_MCB_LITE_HPP_

#include "tap/communication/can/can_bus.hpp"
#include "tap/communication/can/can_rx_handler.hpp"
#include "tap/communication/serial/dji_serial.hpp"
#include "tap/communication/serial/uart.hpp"
#include "tap/drivers.hpp"

#include "can/virtual_can_rx_handler.hpp"
#include "mcb-lite/message_types.hpp"
#include "modm/container/queue.hpp"
#include "sensor-interfaces/analog_handler.hpp"
#include "sensor-interfaces/digital_handler.hpp"
#include "sensor-interfaces/imu_handler.hpp"

namespace src::virtualMCB
{
/**
 * This class is used to communicate with the the main MCB using the UART port.
 * This class handles the sending and receiving of motor data, as well as sending
 * IMU and current sensor data. To use, first call initialize() to set up the port, and then call
 * sendData() to send the data. Call updateSerial() as fast as possible to receive data.
 */
class SerialMCBLite : public tap::communication::serial::DJISerial
{
public:
    SerialMCBLite(tap::Drivers* drivers, tap::communication::serial::Uart::UartPort port);

    void updateIO();

    void sendData();

    void initialize();

    void messageReceiveCallback(const ReceivedSerialMessage& completeMessage) override;

private:
    constexpr static int UART_BAUDRATE = 230'400;

    void sendMotorData(const ReceivedSerialMessage& completeMessage);

    void processDigitalOutputMessage(const ReceivedSerialMessage& completeMessage);

    void processDigitalPinModeMessage(const ReceivedSerialMessage& completeMessage);

    void processPWMPinDutyMessage(const ReceivedSerialMessage& completeMessage);

    void processPWMTimerFrequencyMessage(const ReceivedSerialMessage& completeMessage);

    void processPWMTimerStartedMessage(const ReceivedSerialMessage& completeMessage);

    void processLEDMessage(const ReceivedSerialMessage& completeMessage);

    tap::communication::serial::Uart::UartPort port;

    IMUHandler imuHandler;

    VirtualCanRxHandler canRxHandler;
    modm::can::Message message;

    AnalogHandler analogHandler;

    DigitalHandler digitalHandler;
    DigitalOutputPinMessage digitalOutputMessage;
    DigitalPinModeMessage digitalPinModeMessage;

    PWMPinDutyMessage pwmPinDutyMessage;
    PWNTimerFrequencyMessage pwmTimerFrequencyMessage;
    PWMTimerStartedMessage pwmTimerStartedMessage;

    LEDControlMessage ledStatus;

public:
    // DEBUG VARIABLES
    bool updateIObool = true;

    int receivedMessageCount = 0;

    int receivedIMUMessageCount = 0;

    int receivedCanbus1Message = 0;
    int receivedCanbus2Message = 0;

    int receivedDigitalOutputMessage = 0;
    int receivedDigitalPinModeMessage = 0;

    int receivedPWMPinDutyMessage = 0;
    int receivedPWMTimerFrequencyMessage = 0;
    int receivedPWMTimerStartedMessage = 0;

    int receievedLEDMessage = 0;

    int sendingAMessage = 0;
    int sentAMessage = 0;
};

}  // namespace src::virtualMCB

#endif  // SERIAL_MCB_LITE_HPP_
